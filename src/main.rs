use r#typeof::type_of;
use test_macro::my_macro;

fn main() {
    /*let rml = "<p>je suis un putain de texte</p>";

    let p = Document::create_instance("p");
    let p = p as PElement;

    println!("Hello, world!");*/
    //test::test();
    let mut instance = my_macro!("p");
    println!("{}", type_of(instance));
}

struct UnknownElement {}

impl UnknownElement {
    fn new() -> UnknownElement {
        UnknownElement {}
    }
}

struct PElement {
    text: String
}

impl PElement {
    fn new() -> Box<PElement> {
        let element = Self { text: String::new() };
        Box::new(element)
    }

    fn set_text(&mut self, text: String) {
        self.text = text;
    }

    fn get_text(&self) -> &String {
        &self.text
    }
}